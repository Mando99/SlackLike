export enum IOMessageCommandEnum {
  LOGIN = 'LOGIN',
  UPDATE_SESSION_ID = 'UPDATE_SESSION_ID',

  ADD_USER = 'ADD_USER',

  CREATE_WORKSPACE = 'CREATE_WORKSPACE',
  UPDATE_WORKSPACE = 'UPDATE_WORKSPACE',
  GET_USER_WORKSPACES = 'GET_USER_WORKSPACES',
  LEAVE_WORKSPACE = 'LEAVE_WORKSPACE',
  ADD_WORKSPACE_MEMBER = 'ADD_WORKSPACE_MEMBER',
  DELETE_WORKSPACE_MEMBER = 'DELETE_WORKSPACE_MEMBER',

  CREATE_WORKSPACE_CHANNEL = 'CREATE_WORKSPACE_CHANNEL',
  GET_WORKSPACE_CHANNELS = 'GET_WORKSPACE_CHANNELS',
  GET_WORKSPACE_CHANNEL_MESSAGES = 'GET_WORKSPACE_CHANNEL_MESSAGES',
  ADD_WORKSPACE_CHANNEL_MESSAGE = 'ADD_WORKSPACE_CHANNEL_MESSAGE',

  LOGOUT = 'LOGOUT'
}
