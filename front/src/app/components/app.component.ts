import {Component, OnDestroy, OnInit} from '@angular/core';
import {Container, InteractivityDetect, Main, MoveDirection, OutMode} from "tsparticles";
import {Subscription} from "rxjs";
import {SecurityService} from "../services/security/security.service";
import {UserType} from '../types/models/user.type';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  isAuth = false;
  authUser!: UserType;
  isAuthSubscription!: Subscription;

  particlesOptions = {
    fpsLimit: 60,
    background: {
      color: {
        value: "#302b63"
      }
    },
    particles: {
      number: {
        value: 72,
        density: {
          enable: true,
          value_area: 800
        }
      },
      color: {
        value: ["#2EB67D", "#ECB22E", "#E01E5B", "#36C5F0"]
      },
      shape: {
        type: ["circle"],
        stroke: {
          width: 0,
          color: "#fff"
        },
        polygon: {
          nb_sides: 5
        }
      },
      opacity: {
        value: 1,
        random: false,
        anim: {
          enable: false,
          speed: 1,
          opacity_min: 0.1,
          sync: false
        }
      },
      size: {
        value: 8,
        random: true,
        anim: {
          enable: false,
          speed: 10,
          size_min: 10,
          sync: false
        }
      },
      line_linked: {
        enable: true,
        distance: 150,
        color: "#808080",
        opacity: 0.4,
        width: 1
      },
      move: {
        enable: true,
        speed: 1.5,
        direction: MoveDirection.none,
        random: false,
        straight: false,
        out_mode: OutMode.out,
        bounce: false,
        attract: {
          enable: false,
          rotateX: 600,
          rotateY: 1200
        }
      }
    },
    interactivity: {
      detect_on: InteractivityDetect.canvas,
      events: {
        onhover: {
          enable: true,
          mode: "grab"
        },
        onclick: {
          enable: true,
          mode: "push"
        },
        resize: true
      },
      modes: {
        grab: {
          distance: 140,
          line_linked: {
            opacity: 1
          }
        },
        bubble: {
          distance: 400,
          size: 40,
          duration: 2,
          opacity: 8,
          speed: 3
        },
        repulse: {
          distance: 200,
          duration: 0.4
        },
        push: {
          particles_nb: 4
        },
        remove: {
          particles_nb: 2
        }
      }
    },
    retina_detect: true
  };

  constructor(private securityService: SecurityService) {
  }

  ngOnInit() {
    this.isAuth = this.securityService.isAuth();
    this.authUser = this.securityService.getSessionFromLocalStorage().user;
    this.isAuthSubscription = this.securityService.getUserSessionInLocalStorageSubject().subscribe(() => {
      this.isAuth = this.securityService.isAuth();
      this.authUser = this.securityService.getSessionFromLocalStorage().user;
    });
  }

  particlesLoaded(container: Container): void {
    console.log(container);
  }

  particlesInit(main: Main): void {
    console.log(main);
  }

  logOut(): void {
    this.securityService.logOut();
  }

  ngOnDestroy(): void {
    this.isAuthSubscription?.unsubscribe();
  }

}
