import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {WebsocketService} from "../../../services/websocket/websocket.service";
import {IOMessageCommandEnum} from "../../../enums/i-o-message-command.enum";
import {SecurityService} from 'src/app/services/security/security.service';
import {IOMessageType} from "../../../types/iomessage/IOMessage.type";

@Component({
  selector: 'app-add-workspace',
  templateUrl: './add-workspace.component.html',
  styleUrls: ['./add-workspace.component.css']
})
export class AddWorkspaceComponent implements OnInit {

  public workSpaceForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private securityService: SecurityService, private webSocketService: WebsocketService) {
  }

  ngOnInit(): void {
    this.initSignUpForm();
  }

  private initSignUpForm(): void {
    this.workSpaceForm = this.formBuilder.group({
      name: new FormControl('')
    });
  }

  onSubmitWorkspaceForm(): void {
    if (this.workSpaceForm.valid) {
      const IMessage: IOMessageType = {
        command: IOMessageCommandEnum.CREATE_WORKSPACE,
        sessionID: this.securityService.getSessionFromLocalStorage().session,
        user: this.securityService.getSessionFromLocalStorage().user,
        workspace: {
          name: this.workSpaceForm.value.name
        }
      };
      this.webSocketService.sendIMessage(IMessage);
      this.workSpaceForm.reset();
    }
  }

}
