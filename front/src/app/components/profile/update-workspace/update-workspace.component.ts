import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {SecurityService} from "../../../services/security/security.service";
import {WebsocketService} from "../../../services/websocket/websocket.service";
import {IOMessageType} from "../../../types/iomessage/IOMessage.type";
import {IOMessageCommandEnum} from "../../../enums/i-o-message-command.enum";
import {WorkspaceType} from "../../../types/models/workspace.type";

@Component({
  selector: 'app-update-workspace',
  templateUrl: './update-workspace.component.html',
  styleUrls: ['./update-workspace.component.css']
})
export class UpdateWorkspaceComponent implements OnInit {

  @Input()
  public currentWorkspace!: WorkspaceType | undefined;

  public updateWorkSpaceForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private securityService: SecurityService, private webSocketService: WebsocketService) {
  }

  ngOnInit(): void {
    this.initUpdateWorkSpaceForm();
  }

  private initUpdateWorkSpaceForm(): void {
    this.updateWorkSpaceForm = this.formBuilder.group({
      name: new FormControl((this.currentWorkspace) ? this.currentWorkspace.name : '')
    });
  }

  onSubmitUpdateWorkSpaceForm(): void {
    if (this.updateWorkSpaceForm.valid && this.updateWorkSpaceForm.value.name != this.currentWorkspace?.name) {
      const IMessage: IOMessageType = {
        command: IOMessageCommandEnum.UPDATE_WORKSPACE,
        sessionID: this.securityService.getSessionFromLocalStorage().session,
        user: { id: this.securityService.getSessionFromLocalStorage().user.id },
        workspace: {
          id: parseInt(localStorage.getItem("current-workspace-id") || '0', 10),
          name: this.updateWorkSpaceForm.value.name
        }
      };
      this.webSocketService.sendIMessage(IMessage);
      this.updateWorkSpaceForm.reset();
    }
  }

}
