import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {WebsocketService} from "../../services/websocket/websocket.service";
import {IOMessageCommandEnum} from "../../enums/i-o-message-command.enum";
import {Subscription} from "rxjs";
import {WorkspaceType} from 'src/app/types/models/workspace.type';
import {SecurityService} from 'src/app/services/security/security.service';
import {IOMessageType} from "../../types/iomessage/IOMessage.type";
import {WorkspaceChannelType} from 'src/app/types/models/workspace-channel.type';
import {OMessageStatusEnum} from "../../enums/o-message-status.enum";
import {AlertType} from "../../types/alert/alert.type";
import {UserType} from "../../types/models/user.type";
import {WorkspaceMemberType} from 'src/app/types/models/workspace-member.type';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {

  public channelsMenuOpen = true;

  public modalIsOpen = false;
  public modalContent = "";

  public localStorageCurrentWorkspaceIDKey = "current-workspace-id";
  public localStorageCurrentChannelIDKey = "current-channel-id";

  public workspaces: WorkspaceType[] = [];
  public currentWorkspace!: WorkspaceType | undefined;
  public currentWorkspaceMember!: WorkspaceMemberType;
  public currentChannel!: WorkspaceChannelType | undefined;

  public showAlert: boolean = false;
  public alert!: AlertType;
  public successMessage: string = "SUCCESS";
  public errorMessage: string = "ERROR something went wrong try again.";

  private subscription!: Subscription;

  constructor(private formBuilder: FormBuilder, private securityService: SecurityService, private webSocketService: WebsocketService) {
  }

  ngOnInit(): void {
    const iMessageGetWorkspaces: IOMessageType = {
      command: IOMessageCommandEnum.GET_USER_WORKSPACES,
      sessionID: this.securityService.getSessionFromLocalStorage().session,
      user: this.securityService.getSessionFromLocalStorage().user,
    };
    this.webSocketService.sendIMessage(iMessageGetWorkspaces);

    this.subscription = this.webSocketService.getWebSocket().subscribe(msg => {
        console.log('message received: ' + JSON.stringify(msg));
        if (msg.status == OMessageStatusEnum.SUCCESS) {
          if (msg.command == IOMessageCommandEnum.CREATE_WORKSPACE) {
            this.workspaces.push(msg.workspace);
            this.closeModal();
            this.showAlertMsg("SUCCESS", "Workspace '" + msg.workspace.name + "' successfully created !");
          }

          if (msg.command == IOMessageCommandEnum.UPDATE_WORKSPACE) {
            // @ts-ignore
            this.workspaces.find(workspace => workspace.id == msg.workspace.id).name = msg.workspace.name;
            let formerName = "";
            if (this.currentWorkspace && this.currentWorkspace?.name) {
              formerName = this.currentWorkspace?.name;
              this.currentWorkspace.name = msg.workspace.name;
            }

            if (msg?.userWhoSendIMessageID == this.securityService.getSessionFromLocalStorage().user.id) {
              this.closeModal();
              this.showAlertMsg("SUCCESS", "Workspace '" + formerName + "' successfully updated to '" + this.currentWorkspace?.name + "' !");
            } else if (msg.workspace.id == this.currentWorkspace?.id) {
              this.showAlertMsg("SUCCESS", "Workspace '" + formerName + "' updated to '" + this.currentWorkspace?.name + "' !");
            }
          }

          if (msg.command == IOMessageCommandEnum.UPDATE_SESSION_ID) {
            this.securityService.setUserSessionInLocalStorage({user: msg.user, session: msg.sessionID});
          }

          if (msg.command == IOMessageCommandEnum.GET_USER_WORKSPACES) {
            this.workspaces = msg.user.workspaces;
            const localStorageCurrentWorkspaceID = parseInt(localStorage.getItem(this.localStorageCurrentWorkspaceIDKey) || '0', 10);

            if (localStorageCurrentWorkspaceID != 0) {
              this.workspaces.forEach(workspace => {
                if (workspace.id == localStorageCurrentWorkspaceID)
                  this.getWorkspaceChannels(workspace);
              });
            }
          }

          if (msg.command == IOMessageCommandEnum.CREATE_WORKSPACE_CHANNEL) {
            this.currentWorkspace?.channels?.push(msg.workspaceChannel);
            if (msg?.userWhoSendIMessageID == this.securityService.getSessionFromLocalStorage().user.id) {
              this.closeModal();
              this.showAlertMsg("SUCCESS", "Channel '" + msg.workspaceChannel.name + "' successfully created !");
            }
          }

          if (msg.command == IOMessageCommandEnum.GET_WORKSPACE_CHANNELS) {
            if (msg?.userWhoSendIMessageID == this.securityService.getSessionFromLocalStorage().user.id) {
              this.currentWorkspace = msg.workspace;
              if (this.currentWorkspace?.id) {
                localStorage.setItem(this.localStorageCurrentWorkspaceIDKey, this.currentWorkspace.id.toString());
              }

              this.currentWorkspace?.members?.forEach(member => {
                if (member.member.id == this.securityService.getSessionFromLocalStorage().user.id) {
                  this.currentWorkspaceMember = member;
                  console.log(this.currentWorkspaceMember);
                }
              });

              const localStorageCurrentChannelID = parseInt(localStorage.getItem(this.localStorageCurrentChannelIDKey) || '0', 10);
              if (this.currentWorkspace?.channels && localStorageCurrentChannelID != 0) {
                this.currentWorkspace.channels.forEach(channel => {
                  if (channel.id == localStorageCurrentChannelID)
                    this.getChannelMessages(channel);
                });
              }
              console.log("Current workspace : " + JSON.stringify(this.currentWorkspace));
            }
          }

          if (msg.command == IOMessageCommandEnum.GET_WORKSPACE_CHANNEL_MESSAGES) {
            this.currentChannel = msg.workspaceChannel;
            if (this.currentChannel?.id) {
              localStorage.setItem(this.localStorageCurrentChannelIDKey, this.currentChannel.id.toString());
            }
            console.log("Current channel : " + JSON.stringify(this.currentChannel));
          }

          if (msg.command == IOMessageCommandEnum.ADD_WORKSPACE_CHANNEL_MESSAGE) {
            if (this.currentChannel && msg.workspaceChannel && this.currentChannel.messages && this.currentChannel?.id == msg.workspaceChannel.id) {
              this.currentChannel.messages.push(msg.workspaceChannelMessage);
              console.log("Current channel messages : " + JSON.stringify(msg.workspaceChannel.messages));
            }
          }

          if (msg.command == IOMessageCommandEnum.ADD_WORKSPACE_MEMBER) {
            const user: UserType = <UserType>msg.user;
            if (msg?.userWhoSendIMessageID == this.securityService.getSessionFromLocalStorage().user.id) {
              this.currentWorkspace?.members?.push(msg.workspaceMember);
              this.closeModal();
              this.showAlertMsg("SUCCESS", "User " + user.firstname + ' ' + user.lastname + " successfully added !");
            } else if (msg?.workspaceMember.member.id == this.securityService.getSessionFromLocalStorage().user.id) {
              this.workspaces.push(msg.workspace)
              this.showAlertMsg("SUCCESS", "You were added in workspace : " + msg.workspace.name);
            } else if (msg.workspaceMember.workspace.id == this.currentWorkspace?.id) {
              this.currentWorkspace?.members?.push(msg.workspaceMember);
              this.showAlertMsg("SUCCESS", "User " + msg.workspaceMember.member.firstname + ' ' + msg.workspaceMember.member.lastname + " added !");
            }
          }

          if (msg.command == IOMessageCommandEnum.DELETE_WORKSPACE_MEMBER) {
            if (msg?.userWhoSendIMessageID == this.securityService.getSessionFromLocalStorage().user.id) {
              // @ts-ignore
              this.currentWorkspace?.members = this.currentWorkspace?.members?.filter(m => m.member.id != msg.workspaceMember.member.id);
              this.closeModal();
              this.showAlertMsg("SUCCESS", "User " + msg.workspaceMember.member.firstname + ' ' + msg.workspaceMember.member.lastname + " successfully deleted !");
            } else if (msg?.workspaceMember.member.id == this.securityService.getSessionFromLocalStorage().user.id) {
              this.showAlertMsg("SUCCESS", "You have been deleted from workspace " + msg.workspaceMember.workspace.name + ".");
              this.workspaces = this.workspaces.filter(w => w.id != msg.workspaceMember.workspace.id);
              if (msg.workspaceMember.workspace.id == this.currentWorkspace?.id) {
                this.currentWorkspace = undefined;
              }
            } else if (msg.workspaceMember.workspace.id == this.currentWorkspace?.id) {
              // @ts-ignore
              this.currentWorkspace?.members = this.currentWorkspace?.members?.filter(m => m.member.id != msg.workspaceMember.member.id);
              this.showAlertMsg("SUCCESS", "User " + msg.workspaceMember.member.firstname + ' ' + msg.workspaceMember.member.lastname + " deleted !");
            }
          }
        } else {
          if (msg.userWhoSendIMessageID == this.securityService.getSessionFromLocalStorage().user.id) {
            this.showAlertMsg("ERROR", "");
          }
        }
      },
      err => {
        console.log(err);
        this.leaveWorkspace();
        this.securityService.removeSessionFromLocalStorage();
      }
    );
  }

  getWorkspaceChannels(workspace: WorkspaceType): void {
    if (!this.currentWorkspace || (this.currentWorkspace && this.currentWorkspace.id != workspace.id)) {
      this.leaveWorkspace();
      const IMessageGetWorkspaceChannels: IOMessageType = {
        command: IOMessageCommandEnum.GET_WORKSPACE_CHANNELS,
        sessionID: this.securityService.getSessionFromLocalStorage().session,
        user: { id: this.securityService.getSessionFromLocalStorage().user.id },
        workspace: { id: workspace.id, name: workspace.name  }
      };
      this.webSocketService.sendIMessage(IMessageGetWorkspaceChannels);
    }
  }

  leaveWorkspace(): void {
    if (this.currentWorkspace) {
      const IMessageLeaveWorkspace: IOMessageType = {
        command: IOMessageCommandEnum.LEAVE_WORKSPACE,
        sessionID: this.securityService.getSessionFromLocalStorage().session,
        user: { id: this.securityService.getSessionFromLocalStorage().user.id },
        workspace: { id: this.currentWorkspace.id }
      };
      this.webSocketService.sendIMessage(IMessageLeaveWorkspace);
      this.currentWorkspace = undefined;
      this.currentChannel = undefined;
    }
  }

  getChannelMessages(channel: WorkspaceChannelType): void {
    const IMessageGetChannel: IOMessageType = {
      command: IOMessageCommandEnum.GET_WORKSPACE_CHANNEL_MESSAGES,
      sessionID: this.securityService.getSessionFromLocalStorage().session,
      user: { id: this.securityService.getSessionFromLocalStorage().user.id },
      workspaceChannel: { id: channel.id, name: channel.name, workspace: { id: channel.workspace.id } }
    };
    this.webSocketService.sendIMessage(IMessageGetChannel);
  }

  public openModal(modalContent: string): void {
    this.modalContent = modalContent;
    this.modalIsOpen = true;
  }

  public closeModal(): void {
    this.modalIsOpen = false;
  }

  private showAlertMsg(type: "ERROR" | "SUCCESS", personalizedMessage: string): void {
    this.showAlert = true;

    if (type == "SUCCESS") {
      this.alert = {
        type: "SUCCESS",
        message: (personalizedMessage == '') ? this.successMessage : personalizedMessage
      };
    } else {
      this.alert = {
        type: "ERROR",
        message: (personalizedMessage == '') ? this.errorMessage : personalizedMessage
      };
    }

    setTimeout(() => {
      this.showAlert = false;
    }, 5000);
  }

  ngOnDestroy(): void {
    console.log("Profile destroy !");
    this.leaveWorkspace();
  }

}
