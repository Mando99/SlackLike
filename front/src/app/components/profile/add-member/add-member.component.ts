import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {AlertType} from "../../../types/alert/alert.type";
import {WebsocketService} from "../../../services/websocket/websocket.service";
import {IOMessageCommandEnum} from "../../../enums/i-o-message-command.enum";
import {WorkSpaceMemberRoleEnum} from "../../../enums/workspace-member-role.enum";
import {IOMessageType} from "../../../types/iomessage/IOMessage.type";
import {WorkspaceType} from "../../../types/models/workspace.type";
import {SecurityService} from "../../../services/security/security.service";

@Component({
  selector: 'app-add-member',
  templateUrl: './add-member.component.html',
  styleUrls: ['./add-member.component.css']
})
export class AddMemberComponent implements OnInit {

  @Input()
  currentWorkspace!: WorkspaceType;

  public workSpaceMemberForm!: FormGroup;

  public alert!: AlertType;

  constructor(private formBuilder: FormBuilder, private webSocketService: WebsocketService, private securityService: SecurityService) {
  }

  ngOnInit(): void {
    this.initWorkSpaceMemberForm();
  }

  private initWorkSpaceMemberForm(): void {
    this.workSpaceMemberForm = this.formBuilder.group({
      email: new FormControl('')
    });
  }

  onSubmitWorkSpaceMemberForm(): void {
    if (this.workSpaceMemberForm.valid) {
      const workSpaceMemberFormValue = this.workSpaceMemberForm.value;

      const iMessage: IOMessageType = {
        command: IOMessageCommandEnum.ADD_WORKSPACE_MEMBER,
        workspaceMember: {
          role: WorkSpaceMemberRoleEnum.MEMBER,
          workspace: { id: this.currentWorkspace.id,  name: this.currentWorkspace.name },
          member: {
            email: workSpaceMemberFormValue.email,
          }
        },
        sessionID: this.securityService.getSessionFromLocalStorage().session,
        user: { id: this.securityService.getSessionFromLocalStorage().user.id },
      };

      this.webSocketService.sendIMessage(iMessage);
      this.workSpaceMemberForm.reset();
    }
  }

}
