import {WorkspaceChannelMessageType} from "./workspace-channel-message.type";
import {AbstractModelType} from "./abstract-model.type";
import {WorkspaceType} from "./workspace.type";

export type WorkspaceChannelType = AbstractModelType & {
  name?: string,
  workspace: WorkspaceType,
  messages?: WorkspaceChannelMessageType[]
}
