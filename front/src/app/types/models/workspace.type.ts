import { WorkspaceChannelType } from "./workspace-channel.type";
import {AbstractModelType} from "./abstract-model.type";
import { WorkspaceMemberType } from "./workspace-member.type";

export type WorkspaceType = AbstractModelType &{
  name?: string,
  members?: WorkspaceMemberType[],
  channels?: WorkspaceChannelType[]
}
