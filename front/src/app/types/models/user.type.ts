import {AbstractModelType} from "./abstract-model.type";

export type UserType = AbstractModelType & {
  firstname?: string,
  lastname?: string,
  email?: string,
  password?: string
}
