import {AbstractModelType} from "./abstract-model.type";
import { WorkspaceMemberType } from "./workspace-member.type";
import {WorkspaceChannelType} from "./workspace-channel.type";

export type WorkspaceChannelMessageType = AbstractModelType & {
  content?: string,
  addedAt?: Date,
  channel?: WorkspaceChannelType,
  member?: WorkspaceMemberType
}
