# SlackLike

**De novembre 2021 à janvier 2022**

Le projet SlackLike est un projet qui vise à permettre aux étudiants de la licence générale informatique L3 DANT (Sorbonne Université) d'appliquer le cours programmation objet Java en codant un équivalent de l'application Slack (https://slack.com/).

### Cahier des charges de la mission
- <a href="docs/specifications.pdf">docs/specifications.pdf</a>

### Exécuter l'application
- <a href="back/README.md">back/README.md</a>
- <a href="front/README.md">front/README.md</a>
