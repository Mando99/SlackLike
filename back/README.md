# Back - SlackLike

## Exigences

- Maven (https://maven.apache.org/)
- Mysql (https://www.mysql.com/)

## Exécuter

```sh
cd back
```

```sh
mvn clean install
```

```sh
java -jar target/SlackLike-1.0-SNAPSHOT.jar
```

## TEST ENDPOINTS

```sh
npm install -g wscat
```
```sh
wscat -c "ws://127.0.0.1:8080/"
```

#### Exemple

```sh
> { command: 'ADD_USER', user: { email: 'dalil.chablis@gmail.com', password: '123' } }
```
