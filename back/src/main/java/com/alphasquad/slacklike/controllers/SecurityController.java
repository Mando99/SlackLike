package com.alphasquad.slacklike.controllers;

import com.alphasquad.slacklike.core.iomessage.IOMessageCommand;
import com.alphasquad.slacklike.core.iomessage.OMessage;
import com.alphasquad.slacklike.core.iomessage.OMessageStatus;
import com.alphasquad.slacklike.core.server.Server;
import com.alphasquad.slacklike.core.server.Session;
import com.alphasquad.slacklike.models.User;
import com.alphasquad.slacklike.repositories.CompositeRepository;
import com.alphasquad.slacklike.utils.UPasswordEncoder;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class SecurityController {

    private final CompositeRepository<User> userRepository = new CompositeRepository<>(User.class);

    public void login(Session session, User user) {
        String email = user.getEmail();
        String password = user.getPassword();

        HashMap<String, String> userExistCondition = new HashMap<>();
        userExistCondition.put("email", email);
        userExistCondition.put("password", UPasswordEncoder.encode(password));
        List<User> findUser = this.userRepository.findBy(userExistCondition);
        boolean userExist = findUser.size() > 0;

        OMessage oMessage = new OMessage();
        oMessage.setCommand(IOMessageCommand.LOGIN);
        oMessage.setStatus((userExist) ? OMessageStatus.SUCCESS : OMessageStatus.ERROR);
        if (userExist) {
            oMessage.setUser(findUser.get(0));
            oMessage.setSessionID(session.getId());
            Server.connectedUsers.put(findUser.get(0).getId(), session);
            System.out.println("SecurityController : login() -> Server.connectedUsers => " + Server.connectedUsers);
        }

        session.sendOMessage(oMessage);
    }

    public User getUserIfIsAuthorized(User user, String sessionIDFromIMessage, Session newSession) {
        System.out.println("SecurityController : getUserIfIsAuthorized() -> Server.connectedUsers => " + Server.connectedUsers);
        Session userSession = Server.connectedUsers.get(user.getId());

        boolean userAuthorized = userSession != null && Objects.equals(userSession.getId(), sessionIDFromIMessage);

        if (userAuthorized && !Objects.equals(userSession.getId(), newSession.getId())) {
            Server.connectedUsers.remove(user.getId());
            Server.connectedUsers.put(user.getId(), newSession);
            System.out.println("SecurityController : getUserIfIsAuthorized() -> Server.connectedUsers (User session replaced) => " + Server.connectedUsers);

            OMessage oMessage = new OMessage();
            oMessage.setCommand(IOMessageCommand.UPDATE_SESSION_ID);
            oMessage.setSessionID(newSession.getId());
            oMessage.setUser(user);
            oMessage.setStatus(OMessageStatus.SUCCESS);

            newSession.sendOMessage(oMessage);
        }

        return (userAuthorized) ? user : null;
    }

    public void logOut(Long userID) {
        Server.connectedUsers.remove(userID);
        System.out.println("SecurityController : logOut() -> Server.connectedUsers => " + Server.connectedUsers);
        System.out.println("SecurityController : logOut() -> Server.openedWorkspaces => " + Server.openedWorkspaces);
    }

}
