package com.alphasquad.slacklike.controllers;

import com.alphasquad.slacklike.core.iomessage.IOMessageCommand;
import com.alphasquad.slacklike.core.iomessage.OMessage;
import com.alphasquad.slacklike.core.iomessage.OMessageStatus;
import com.alphasquad.slacklike.core.server.Server;
import com.alphasquad.slacklike.core.server.Session;
import com.alphasquad.slacklike.models.*;
import com.alphasquad.slacklike.repositories.CompositeRepository;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.*;

public class WorkspaceController {

    private final CompositeRepository<User> userRepository = new CompositeRepository<>(User.class);
    private final CompositeRepository<Workspace> workspaceRepository = new CompositeRepository<>(Workspace.class);
    private final CompositeRepository<WorkspaceMember> workspaceMemberRepository = new CompositeRepository<>(WorkspaceMember.class);
    private final CompositeRepository<WorkspaceChannel> workspaceChannelRepository = new CompositeRepository<>(WorkspaceChannel.class);
    private final CompositeRepository<WorkspaceChannelMessage> workspaceChannelMessageRepository = new CompositeRepository<>(WorkspaceChannelMessage.class);

    public void createWorkspace(Session session, Workspace workSpace, User user) throws InvocationTargetException, IllegalAccessException, IOException {
        Workspace newWorkspace = this.workspaceRepository.add(workSpace);
        System.out.println(newWorkspace);
        WorkspaceMember newWorkspaceMember = this.workspaceMemberRepository.add(new WorkspaceMember(newWorkspace, user, WorkspaceMemberRole.ADMIN));

        OMessage oMessage = new OMessage();
        oMessage.setCommand(IOMessageCommand.CREATE_WORKSPACE);
        oMessage.setSessionID(session.getId());
        oMessage.setStatus((newWorkspace != null && newWorkspaceMember != null) ? OMessageStatus.SUCCESS : OMessageStatus.ERROR);
        oMessage.setWorkspace(newWorkspace);

        session.sendOMessage(oMessage);
    }

    public void updateWorkspace(Workspace workSpace, User user) {
        HashMap<String, String> userIsAdminCondition = new HashMap<>();
        userIsAdminCondition.put("workspace_id", workSpace.getId().toString());
        userIsAdminCondition.put("member_id", user.getId().toString());
        userIsAdminCondition.put("role", WorkspaceMemberRole.ADMIN.toString());
        List<WorkspaceMember> userIsAdmin = this.workspaceMemberRepository.findBy(userIsAdminCondition);

        if (userIsAdmin.size() > 0) {

            Workspace updatedWorkspace = this.workspaceRepository.update(workSpace);
            System.out.println(updatedWorkspace);

            OMessage oMessage = new OMessage();
            oMessage.setCommand(IOMessageCommand.UPDATE_WORKSPACE);
            if (updatedWorkspace != null) {
                oMessage.setStatus(OMessageStatus.SUCCESS);
                workSpace.setName(updatedWorkspace.getName());
                oMessage.setWorkspace(workSpace);
            } else {
                oMessage.setStatus(OMessageStatus.ERROR);
            }

            Server.broadcastToWorkspaceMembers(workSpace.getId(), user.getId(), oMessage);
        }
    }

    public void getUserWorkSpaces(Session session, User user) {
        // TODO : Improve
        List<Workspace> workspaces = this.workspaceRepository.findWithCustomRequest("SELECT workspaces.* FROM workspaces, workspace_members WHERE workspaces.id = workspace_members.workspace_id AND workspace_members.member_id = '" + user.getId() + "';");
        user.setWorkspaces(workspaces);

        OMessage oMessage = new OMessage();
        oMessage.setCommand(IOMessageCommand.GET_USER_WORKSPACES);
        oMessage.setSessionID(session.getId());
        oMessage.setStatus((workspaces != null) ? OMessageStatus.SUCCESS : OMessageStatus.ERROR);
        oMessage.setUser(user);

        session.sendOMessage(oMessage);
    }

    public void addWorkspaceChannel(WorkspaceChannel channel, User user) throws InvocationTargetException, IllegalAccessException {
        List<WorkspaceMember> userIsAdmin = userWorkspaceAdmin(user, channel.getWorkspace());

        if (userIsAdmin.size() > 0) {
            WorkspaceChannel newChannel = this.workspaceChannelRepository.add(channel);

            OMessage oMessage = new OMessage();
            oMessage.setCommand(IOMessageCommand.CREATE_WORKSPACE_CHANNEL);
            oMessage.setStatus((newChannel != null) ? OMessageStatus.SUCCESS : OMessageStatus.ERROR);
            if (newChannel != null) {
                channel.setId(newChannel.getId());
                oMessage.setWorkspaceChannel(channel);
            }

            Server.broadcastToWorkspaceMembers(channel.getWorkspace().getId(), user.getId(), oMessage);
        }
    }

    public void getWorkspaceChannels(Workspace workspace, User user) {
        HashMap<String, String> workspaceCondition = new HashMap<>();
        workspaceCondition.put("workspace_id", workspace.getId().toString());
        List<WorkspaceChannel> workspaceChannels = this.workspaceChannelRepository.findBy(workspaceCondition);
        workspace.setChannels(workspaceChannels);

        HashMap<String, String> workspaceMembersCondition = new HashMap<>();
        workspaceMembersCondition.put("workspace_id", workspace.getId().toString());
        List<WorkspaceMember> workspaceMembers = this.workspaceMemberRepository.findBy(workspaceMembersCondition);
        workspace.setMembers(workspaceMembers);

        OMessage oMessage = new OMessage();
        oMessage.setCommand(IOMessageCommand.GET_WORKSPACE_CHANNELS);
        oMessage.setStatus((workspaceChannels != null) ? OMessageStatus.SUCCESS : OMessageStatus.ERROR);
        oMessage.setWorkspace(workspace);

        Set<Long> openedWorkspace = Server.openedWorkspaces.get(workspace.getId());
        if (openedWorkspace != null) {
            openedWorkspace.add(user.getId());
            Server.openedWorkspaces.put(workspace.getId(), openedWorkspace);
        } else {
            Set<Long> userIDs = new HashSet<>();
            userIDs.add(user.getId());
            Server.openedWorkspaces.put(workspace.getId(), userIDs);
        }

        System.out.println("WorkspaceController : getWorkspaceChannels() -> Server.openedWorkspaces => " + Server.openedWorkspaces);
        Server.broadcastToWorkspaceMembers(workspace.getId(), user.getId(), oMessage);
    }

    public void leaveWorkspace(Workspace workspace, User user) {
       Set<Long> userIDs = Server.openedWorkspaces.get(workspace.getId());
       userIDs.removeIf(userID -> Objects.equals(userID, user.getId()));

       if (userIDs.size() == 0) {
           Server.openedWorkspaces.remove(workspace.getId());
       } else {
           OMessage oMessage = new OMessage();
           oMessage.setCommand(IOMessageCommand.LEAVE_WORKSPACE);
           oMessage.setStatus(OMessageStatus.SUCCESS);
           oMessage.setUser(user);
           oMessage.setWorkspace(workspace);
           Server.broadcastToWorkspaceMembers(workspace.getId(), user.getId(), oMessage);
       }

        System.out.println("WorkspaceController : leaveWorkspace() -> Server.openedWorkspaces => " + Server.openedWorkspaces);
    }

    public void addWorkspaceMember(WorkspaceMember workspaceMember, User user) throws InvocationTargetException, IllegalAccessException {
        List<WorkspaceMember> userIsAdmin = userWorkspaceAdmin(user, workspaceMember.getWorkspace());

        if (userIsAdmin.size() > 0) {
            HashMap<String, String> newWorkspaceMemberIsAppUserCondition = new HashMap<>();
            newWorkspaceMemberIsAppUserCondition.put("email", workspaceMember.getMember().getEmail());
            List<User> newWorkspaceMemberIsAppUser = this.userRepository.findBy(newWorkspaceMemberIsAppUserCondition);

            OMessage oMessage = new OMessage();
            oMessage.setCommand(IOMessageCommand.ADD_WORKSPACE_MEMBER);

            WorkspaceMember newWorkspaceMember = null;
            if (newWorkspaceMemberIsAppUser.size() > 0) {
                HashMap<String, String> memberAlreadyExistCondition = new HashMap<>();
                memberAlreadyExistCondition.put("member_id", newWorkspaceMemberIsAppUser.get(0).getId().toString());
                memberAlreadyExistCondition.put("workspace_id", workspaceMember.getWorkspace().getId().toString());
                List<WorkspaceMember> findMember = this.workspaceMemberRepository.findBy(memberAlreadyExistCondition);
                boolean memberExist = findMember.size() > 0;
                workspaceMember.setMember(newWorkspaceMemberIsAppUser.get(0));
                workspaceMember.setRole(WorkspaceMemberRole.MEMBER);
                if (!memberExist)
                    newWorkspaceMember = this.workspaceMemberRepository.add(workspaceMember);
            }

            oMessage.setStatus((newWorkspaceMember != null) ? OMessageStatus.SUCCESS : OMessageStatus.ERROR);
            if (newWorkspaceMember != null) {
                workspaceMember.setId(newWorkspaceMember.getId());
                oMessage.setUser(newWorkspaceMemberIsAppUser.get(0));
                oMessage.setWorkspaceMember(workspaceMember);
                oMessage.setWorkspace(workspaceMember.getWorkspace());

                Set<Long> openedWorkspace = Server.openedWorkspaces.get(workspaceMember.getWorkspace().getId());
                if (openedWorkspace != null) {
                    openedWorkspace.add(workspaceMember.getMember().getId());
                    Server.openedWorkspaces.put(workspaceMember.getMember().getId(), openedWorkspace);
                }
            }

            Server.broadcastToWorkspaceMembers(workspaceMember.getWorkspace().getId(), user.getId(), oMessage);
        }
    }

    public void deleteWorkspaceMember(WorkspaceMember workspaceMember, User user) {
        // TODO (bug) : If a user has sent message, it cannot be deleted (foreign key).

        List<WorkspaceMember> userIsAdmin = userWorkspaceAdmin(user, workspaceMember.getWorkspace());

        if (!workspaceMember.getMember().getId().equals(user.getId()) && userIsAdmin.size() > 0) {
            this.workspaceMemberRepository.delete(workspaceMember);

            OMessage oMessage = new OMessage();
            oMessage.setCommand(IOMessageCommand.DELETE_WORKSPACE_MEMBER);
            oMessage.setStatus(OMessageStatus.SUCCESS);
            oMessage.setWorkspaceMember(workspaceMember);

            Server.broadcastToWorkspaceMembers(workspaceMember.getWorkspace().getId(), user.getId(), oMessage);
        }
    }

    public void getWorkspaceChannelMessages(Session session, WorkspaceChannel workspaceChannel) {
        // TODO : User authorized ?

        HashMap<String, String> workspaceChannelMessagesCondition = new HashMap<>();
        workspaceChannelMessagesCondition.put("workspace_channel_id", workspaceChannel.getId().toString());
        List<WorkspaceChannelMessage> workspaceChannelMessages = this.workspaceChannelMessageRepository.findBy(workspaceChannelMessagesCondition);

        // TODO : Improve
        workspaceChannelMessages.forEach(message -> {
            List<User> member = this.userRepository.findWithCustomRequest("SELECT * FROM users, workspace_members WHERE users.id = workspace_members.member_id AND workspace_members.id = " + message.getMember().getId());
            message.getMember().setMember(member.get(0));
        });

        OMessage oMessage = new OMessage();
        oMessage.setCommand(IOMessageCommand.GET_WORKSPACE_CHANNEL_MESSAGES);
        oMessage.setStatus(OMessageStatus.SUCCESS);
        workspaceChannel.setMessages(workspaceChannelMessages);
        oMessage.setWorkspaceChannel(workspaceChannel);

        session.sendOMessage(oMessage);
    }

    public void addWorkspaceChannelMessage(Workspace workspace, WorkspaceChannel workspaceChannel, WorkspaceChannelMessage channelMessage, User user) throws InvocationTargetException, IllegalAccessException {
        HashMap<String, String> userIsWorkspaceMemberCondition = new HashMap<>();
        userIsWorkspaceMemberCondition.put("member_id", user.getId().toString());
        List<WorkspaceMember> userIsWorkspaceMember = this.workspaceMemberRepository.findBy(userIsWorkspaceMemberCondition);

        if (userIsWorkspaceMember != null && userIsWorkspaceMember.size() > 0) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = new Date();
            channelMessage.setAddedAt(formatter.format(date));
            channelMessage.setMember(userIsWorkspaceMember.get(0));
            WorkspaceChannelMessage newMessage = this.workspaceChannelMessageRepository.add(channelMessage);

            OMessage oMessage = new OMessage();
            oMessage.setCommand(IOMessageCommand.ADD_WORKSPACE_CHANNEL_MESSAGE);
            oMessage.setStatus((newMessage != null) ? OMessageStatus.SUCCESS : OMessageStatus.ERROR);

            if (newMessage != null) {
                oMessage.setWorkspaceChannel(workspaceChannel);
                oMessage.setWorkspaceChannelMessage(newMessage);
                Server.broadcastToWorkspaceMembers(workspace.getId(), user.getId(), oMessage);
            }
        }
    }

    private List<WorkspaceMember> userWorkspaceAdmin(User user, Workspace workspace) {
        HashMap<String, String> userIsAdminCondition = new HashMap<>();
        userIsAdminCondition.put("member_id", user.getId().toString());
        userIsAdminCondition.put("workspace_id", workspace.getId().toString());
        userIsAdminCondition.put("role", WorkspaceMemberRole.ADMIN.toString());
        return this.workspaceMemberRepository.findBy(userIsAdminCondition);
    }

}
