package com.alphasquad.slacklike.models;

import com.alphasquad.slacklike.core.annotations.DBColumn;
import com.alphasquad.slacklike.core.annotations.DBColumnOneToMany;
import com.alphasquad.slacklike.core.annotations.DBTable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@DBTable(name = "workspaces")
public class Workspace extends AbstractModel {

    @DBColumn(name = "id", options = "INT PRIMARY KEY NOT NULL AUTO_INCREMENT")
    private Long id;

    @DBColumn(name = "name", options = "VARCHAR(255)")
    private String name;

    @DBColumnOneToMany(name = "workspace_members", joinColumnName = "member_id")
    private List<WorkspaceMember> members = new ArrayList<>();

    @DBColumnOneToMany(name = "workspace_channels", joinColumnName = "channel_id")
    private List<WorkspaceChannel> channels = new ArrayList<>();

}
