package com.alphasquad.slacklike.models;

import com.alphasquad.slacklike.core.annotations.DBColumn;
import com.alphasquad.slacklike.core.annotations.DBColumnOneToMany;
import com.alphasquad.slacklike.core.annotations.DBColumnManyToOne;
import com.alphasquad.slacklike.core.annotations.DBTable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@DBTable(name = "workspace_channels")
public class WorkspaceChannel extends AbstractModel {

    @DBColumn(name = "id", options = "INT PRIMARY KEY NOT NULL AUTO_INCREMENT")
    private Long id;

    @DBColumn(name = "name", options = "VARCHAR(255)")
    private String name;

    @DBColumnManyToOne(name = "workspace_id", options = "INT", joinTableName = "workspaces", joinColumnName = "id")
    private Workspace workspace;

    @DBColumnOneToMany(name = "workspace_channel_messages", joinColumnName = "workspace_channel_id")
    private List<WorkspaceChannelMessage> messages = new ArrayList<>();

}
