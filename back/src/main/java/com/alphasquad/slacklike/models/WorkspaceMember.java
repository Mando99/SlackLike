package com.alphasquad.slacklike.models;

import com.alphasquad.slacklike.core.annotations.DBColumn;
import com.alphasquad.slacklike.core.annotations.DBColumnManyToOne;
import com.alphasquad.slacklike.core.annotations.DBTable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@DBTable(name = "workspace_members")
public class WorkspaceMember extends AbstractModel {

    @DBColumn(name = "id", options = "INT PRIMARY KEY NOT NULL AUTO_INCREMENT")
    private Long id;

    @DBColumnManyToOne(name = "workspace_id", options = "INT", joinTableName = "workspaces", joinColumnName = "id")
    private Workspace workspace;

    @DBColumnManyToOne(name = "member_id", options = "INT", joinTableName = "users", joinColumnName = "id")
    private User member;

    @DBColumn(name = "role", options = "VARCHAR(15)")
    private WorkspaceMemberRole role;

    public WorkspaceMember(Workspace workspace, User member, WorkspaceMemberRole role) {
        this.workspace = workspace;
        this.member = member;
        this.role = role;
    }

}
