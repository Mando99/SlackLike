package com.alphasquad.slacklike.models;

public enum WorkspaceMemberRole {
    ADMIN,
    MEMBER
}