package com.alphasquad.slacklike.repositories;

import com.alphasquad.slacklike.models.AbstractModel;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;

public interface IRepository<T extends AbstractModel> {

    T add(T model) throws InvocationTargetException, IllegalAccessException;

    T find(Long id);

    List<T> findBy(HashMap<String, String> condition);

    List<T> findAll();

    List<T> findWithCustomRequest(String req);

    T update(T model);

    T delete(T model);

}
