package com.alphasquad.slacklike;

import com.alphasquad.slacklike.core.config.GlobalConfig;
import com.alphasquad.slacklike.core.database.Database;
import com.alphasquad.slacklike.core.server.Server;
import com.alphasquad.slacklike.utils.UFilesResources;

import java.io.InputStream;

public class SlackLike {

    public static void main(String[] args) throws Exception {

        GlobalConfig gc = GlobalConfig.get("application.properties");

        System.out.println(gc.getProperty("application.name") + " started !");

        Database db = Database.get();
        db.initConnection(gc.getProperty("datasource.url"), gc.getProperty("datasource.username"), gc.getProperty("datasource.password"));
        db.createOrUpdateTables();

        if (gc.getProperty("application.mode").equals("dev")) {
            db.loadSQLFiles(new InputStream[]{UFilesResources.getInputStreamFile("data.sql")});
        }

        Server server = new Server(Integer.parseInt(gc.getProperty("server.port")));
        server.run();

    }

}
