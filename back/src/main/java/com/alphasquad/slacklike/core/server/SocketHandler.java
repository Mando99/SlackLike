package com.alphasquad.slacklike.core.server;

import com.alphasquad.slacklike.utils.UIOMessage;
import com.alphasquad.slacklike.utils.UWebSocket;

import java.io.*;
import java.net.Socket;

public class SocketHandler implements Runnable {

    private final Socket socket;
    private final InputStream inputStream;
    private final Session session;
    private final Endpoint endpoint;

    public SocketHandler(Socket socket, Endpoint endpoint) throws IOException {
        this.socket = socket;
        this.inputStream = socket.getInputStream();
        this.session = new Session(socket.getOutputStream());
        this.endpoint = endpoint;
    }

    @Override
    public void run() {
        this.endpoint.onOpen(this.session);
        try {
            while (!this.socket.isClosed()) {
                String message = UWebSocket.decodeFrame(this.inputStream);
                if (message.length() > 0) {
                    this.endpoint.onMessage(this.session, UIOMessage.decode(message));
                }
            }
        } catch (Exception e) {
            this.endpoint.onError(this.session, e);
        } finally {
            try {
                this.socket.close();
                this.endpoint.onClose(this.session);
            } catch (IOException e) {
                this.endpoint.onError(this.session, e);
            }
        }
    }

}