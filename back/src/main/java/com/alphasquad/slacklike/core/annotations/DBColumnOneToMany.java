package com.alphasquad.slacklike.core.annotations;

import java.lang.annotation.*;

@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DBColumnOneToMany {
    String name();
    String joinColumnName();
}