package com.alphasquad.slacklike.core.iomessage;

import com.alphasquad.slacklike.models.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

// No AbstractIOMessage for debugging reasons

@Getter
@Setter
@ToString
public class IMessage {

    private IOMessageCommand command;
    private String sessionID;
    private User user;
    private Workspace workspace;
    private WorkspaceChannel workspaceChannel;
    private WorkspaceChannelMessage workspaceChannelMessage;
    private WorkspaceMember workspaceMember;

}
